#ifndef _Model_H_
#define _Model_H_

//----------------------------------------------------------------------------//
//    Learning OpenGL: ���������� �������� OpenGL                             //
//    ��������� ��������� �������������                                       //
//    Class: Model                                                            //
//    Author: Iason Nikolas                                                   //
//    For any bug/suggestions mail to: iason.nikolas@ece.upatras.gr           //
//----------------------------------------------------------------------------//

#include "triangle.h"
#include "vertex.h"
#include "glut.h"
#include <vector>
#include <string>

typedef Vec3<GLfloat>   Vec3f;
typedef Vertex<GLfloat> Vertex3f;

using namespace std;

/*******************************************************************************
  Model
 ******************************************************************************/

class Model
{
public:
  Model();
  Model(Model& other);

  ~Model();
  
  // Methods
  void LoadOBJ(string filename);
  void LoadPartitions(string filename);
  void LoadAnchorPoints(string filename);
  void WriteBounds(string filename);
  void Draw();

public:
  // Data members
  string filename;
  vector<Vertex3f> vertices;
  vector<Vec3f> normals;
  vector<Triangle> triangles;
  vector<int> anchorPoints;
  vector<Color3> palette;
  
  bool showAnchor;

  friend ostream& operator << (ostream& os, const Model& m);

private:
  // Methods
  void SetColorsToPartitions();
  void SetAdjacentVertices();
  vector<string>& SplitStr(const string &s, char delim, vector<string> &elems);
  vector<string>  SplitStr(const std::string &s, char delim);
};


#endif