#ifndef _Triangle_H_
#define _Triangle_H_

//----------------------------------------------------------------------------//
//    Learning OpenGL: ���������� �������� OpenGL                             //
//    ��������� ��������� �������������                                       //
//    Class: Triangle                                                         //
//    Author: Iason Nikolas                                                   //
//    For any bug/suggestions mail to: iason.nikolas@ece.upatras.gr           //
//----------------------------------------------------------------------------//

#include "vertex.h"
#include "glut.h"

typedef Vec3<GLfloat> Color3;

/*******************************************************************************
  Triangle
 ******************************************************************************/

class Triangle
{
public:
  // ctors
  Triangle():v1(-1), v2(-1), v3(-1), n1(-1), n2(-1), n3(-1), col(Color3()) {}
  Triangle(Triangle& other):v1(other.v1), v2(other.v2), v3(other.v3), n1(other.n1), n2(other.n2), n3(other.n3), col(other.col) {}
  Triangle(const Triangle& other):v1(other.v1), v2(other.v2), v3(other.v3), n1(other.n1), n2(other.n2), n3(other.n3), col(other.col) {}
  Triangle(int v1, int v2, int v3, int n1, int n2, int n3, Color3 col = Color3()):v1(v1), v2(v2), v3(v3), n1(n1), n2(n2), n3(n3), col(col) {}

  ~Triangle(){}

  // Data members
  int v1, v2, v3;
  int n1, n2, n3;
  Color3 col;

  friend std::ostream& operator << (std::ostream& os, const Triangle& t)
  {
    os << " Vertices: [" << t.v1 << "//" << t.n1 << ", " << t.v2 << "//" << t.n2 << ", " << t.v3 << "//" << t.n3 << "] Color: " << t.col << " ";
    return os;
  }
};


#endif