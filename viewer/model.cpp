//----------------------------------------------------------------------------//
//    Learning OpenGL: ���������� �������� OpenGL                             //
//                                                                            //
//    ��������� ��������� �������������                                       //
//                                                                            //
//    Simple OBJ Viewer                                                       //
//----------------------------------------------------------------------------//

#include "model.h"
#include "triangle.h"
#include "vertex.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <random>

/*******************************************************************************
  Implementation of Model methods
 ******************************************************************************/

Model::Model() : vertices(std::vector<Vertex3f>()),
                 triangles(std::vector<Triangle>()),
                 showAnchor(true)
{}

//------------------------------------------------------------------------------

Model::Model(Model& other) : vertices(other.vertices),
                             triangles(other.triangles)
{}

//------------------------------------------------------------------------------

Model::~Model()
{}

//------------------------------------------------------------------------------

void Model::LoadOBJ(string filename)
{
  this->filename = filename;
  cout << "Model " << filename << " is Loading..." << endl;
  ifstream file(filename, ifstream::in);
  string line;
  if (file.is_open())
  {
    int triNum = 0;
    while (getline(file, line))
    {
      // If line is empty go to the next one
      if (line.empty()) continue;
      // Split line in tokens
      vector<string> tokens = SplitStr(line, ' ');
      if (tokens[0] == "v")
      {
        Vertex3f p;
        p.pos.x = stof(tokens[1]);
        p.pos.y = stof(tokens[2]);
        p.pos.z = stof(tokens[3]);
        vertices.push_back(p);
      }
      else if (tokens[0] == "vn")
      {
        Vec3f n;
        n.x = stof(tokens[1]);
        n.y = stof(tokens[2]);
        n.z = stof(tokens[3]);
        normals.push_back(n);
      }
      else if (tokens[0] == "f")
      {
        vector<string> tok1, tok2, tok3;
        tok1 = SplitStr(tokens[1], '/');
        tok2 = SplitStr(tokens[2], '/');
        tok3 = SplitStr(tokens[3], '/');

        int v1 = stoi(tok1[0])-1;
        int v2 = stoi(tok2[0])-1;
        int v3 = stoi(tok3[0])-1;

        triangles.push_back(Triangle(v1, v2, v3, stoi(tok1[2])-1, 
                                     stoi(tok2[2])-1, stoi(tok3[2])-1,
                                     Color3(0.8, 0.8, 0.8)));

        // Push the adjacent triangles to the vertices
        //vertices[v1].adjacentTriangles.push_back(triNum);
        //vertices[v2].adjacentTriangles.push_back(triNum);
        //vertices[v3].adjacentTriangles.push_back(triNum);
        triNum++;
      }
    }
    file.close();
    //SetAdjacentVertices();
    cout << "Model " << filename << " Loaded successfully!!" << endl;
  }
  else
  {
    cout << "Unable to open file: " << filename << endl;
    file.close();
  }
}

//------------------------------------------------------------------------------

void Model::LoadPartitions(string filename)
{
  ifstream file(filename);
  int num, max = 0;
  int i = 0;
  while(file >> num)
  {
    vertices[i++].part = num;
    if(max < num) max = num;
  }
  file.close();

  default_random_engine generator;
  gamma_distribution<GLfloat> distribution(10, 10);

  for(size_t i = 0; i <= max; i++)
  {
    Color3 col(fmod(distribution(generator) / 100., 1.0),
               fmod(distribution(generator) / 100., 1.0),
               fmod(distribution(generator) / 100., 1.0));
    palette.push_back(col);
  }

  SetColorsToPartitions();
}

//------------------------------------------------------------------------------

void Model::SetColorsToPartitions()
{
  for(size_t i = 0; i < triangles.size(); i++)
  {
    if((vertices[triangles[i].v1].part == vertices[triangles[i].v2].part) &&
       (vertices[triangles[i].v2].part == vertices[triangles[i].v3].part))
    {
      triangles[i].col = palette[vertices[triangles[i].v1].part];
    }
    else
      triangles[i].col = Color3();
  }
}

//------------------------------------------------------------------------------

void Model::SetAdjacentVertices()
{
  for (int i = 0; i < vertices.size(); i++)
  {
    Vertex3f& currVertex = vertices[i];
    for (size_t j = 0; j < currVertex.adjacentTriangles.size(); j++)
    {
      Triangle& currTriangle = triangles[currVertex.adjacentTriangles[j]];
      if (i != currTriangle.v1)
        currVertex.adjacentVerticies.push_back(currTriangle.v1);
      if (i != currTriangle.v2)
        currVertex.adjacentVerticies.push_back(currTriangle.v2);
      if (i != currTriangle.v3)
        currVertex.adjacentVerticies.push_back(currTriangle.v3);
    }
    vector<int>::iterator it;
    it = unique(currVertex.adjacentVerticies.begin(),
                currVertex.adjacentVerticies.end());
    currVertex.adjacentVerticies.resize(distance(currVertex.adjacentVerticies.begin(), it));
  }
}

//------------------------------------------------------------------------------

void Model::LoadAnchorPoints(string filename)
{
  ifstream file(filename);
  float num;
  while(file >> num) anchorPoints.push_back((int)num);

  file.close();
}

//------------------------------------------------------------------------------

void Model::Draw()
{
  glBegin(GL_TRIANGLES);
    for(size_t i = 0; i < triangles.size(); i++)
    {
      glColor3fv(triangles[i].col.p);

      glNormal3fv(normals[triangles[i].n1].p);
      glVertex3fv(vertices[triangles[i].v1].pos.p);
      glNormal3fv(normals[triangles[i].n2].p);
      glVertex3fv(vertices[triangles[i].v2].pos.p);
      glNormal3fv(normals[triangles[i].n3].p);
      glVertex3fv(vertices[triangles[i].v3].pos.p);
    }
  glEnd();
  if(showAnchor)
  {
    glColor3f(1.0, 0.0, 0.0);
    GLfloat *center;
    for(size_t i = 0; i < anchorPoints.size(); i++)
    {
      center = vertices[anchorPoints[i]].pos.p;
      glPushMatrix();
        glTranslatef(center[0], center[1], center[2]);
        glutSolidSphere(0.5, 9, 9);
      glPopMatrix();
    }
  }
}

//------------------------------------------------------------------------------

void Model::WriteBounds(string filename)
{
  ofstream file(filename);
  for(size_t i = 0; i < triangles.size(); i++)
  {
    Color3 black;
    if(triangles[i].col == black)
      file << triangles[i].v1 << " " << triangles[i].v2 << " " << triangles[i].v3 << endl;
  }
  file.close();
}

//------------------------------------------------------------------------------

vector<string>& Model::SplitStr(const string &s, char delim, vector<string> &elems)
{
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

//------------------------------------------------------------------------------

vector<string> Model::SplitStr(const std::string &s, char delim)
{
    vector<string> elems;
    SplitStr(s, delim, elems);
    return elems;
}

//------------------------------------------------------------------------------

ostream& operator << (ostream& os, const Model& m)
{
  for(size_t i = 0; i < m.triangles.size(); i++)
  {
    os << m.vertices[m.triangles[i].v1].pos << " " << m.normals[m.triangles[i].n1] << endl;
    os << m.vertices[m.triangles[i].v2].pos << " " << m.normals[m.triangles[i].n2] << endl;
    os << m.vertices[m.triangles[i].v3].pos << " " << m.normals[m.triangles[i].n3] << endl;
  }
  return os;
}