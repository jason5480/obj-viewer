#ifndef _Vec3_H_
#define _Vec3_H_

//----------------------------------------------------------------------------//
//    Learning OpenGL: ���������� �������� OpenGL                             //
//    ��������� ��������� �������������                                       //
//    Class: Templated 3D Vector                                              //
//    Author: Iason Nikolas                                                   //
//    For any bug/suggestions mail to: iason.nikolas@ece.upatras.gr           //
//----------------------------------------------------------------------------//

#include <iostream>
#include <math.h>

/*******************************************************************************
  3D Vector
 ******************************************************************************/

template <typename T>
class Vec3
{
public:
  // ctors
  Vec3();
  Vec3(Vec3& other);
  Vec3(const Vec3& other);
  Vec3(T x, T y, T z);

  ~Vec3();

  // The x, y, z coordinates
  union
  {
    struct {T x, y, z;};
    T p[3];
  };
  // Operators to make life easier
  T&    operator [] (int i) const;
  Vec3& operator =  (const Vec3 other);
  // Basic operation with other vector or itself
  Vec3  operator +  (const Vec3& other) const;
  Vec3  operator -  (const Vec3& other) const;
  Vec3  operator *  (const Vec3& other) const;
  Vec3  operator /  (const Vec3& other) const;
  Vec3  operator -  () const;
  // Basic operations with scalar values
  Vec3  operator *  (const T scalar);
  Vec3  operator /  (const T scalar);
  // Operators that change the vector itself based on an other vector
  Vec3& operator += (const Vec3& other);
  Vec3& operator -= (const Vec3& other);
  Vec3& operator *= (const Vec3& other);
  Vec3& operator /= (const Vec3& other);
  // Operators that change the vector itselfbased on an scalar value
  Vec3& operator *= (const T scalar);
  Vec3& operator /= (const T scalar);
  // Boolean operators
  bool  operator == (const Vec3& other) const;
  bool  operator != (const Vec3& other) const;
  bool  operator <  (const Vec3& other) const;

  // Util methods
  void Set(T x, T y, T z);
  T Length() const;
  T LengthSq() const;
  T Distance(const Vec3& other) const;
  Vec3 Normalized() const;
  void Normalize();
  T Dot(const Vec3& other) const;
  Vec3 Cross(const Vec3& other) const;
  bool Equal(const Vec3& other, T e) const;
  void Print(std::ostream& os) const;
};

// For some more convineience
template <typename T>
Vec3<T> operator * (T a, const Vec3<T> vec);

template <typename T>
std::ostream& operator << (std::ostream& os, const Vec3<T>& vec);

/*******************************************************************************
  Inline Implementation of 3D Vector and other operators
 ******************************************************************************/

// Operators to make life easier
template <typename T>
inline Vec3<T>::Vec3() : x(0), y(0), z(0) {}

template <typename T>
inline Vec3<T>::Vec3(Vec3& other) : x(other.x), y(other.y), z(other.z) {}

template <typename T>
inline Vec3<T>::Vec3(const Vec3& other) : x(other.x), y(other.y), z(other.z) {}

template <typename T>
inline Vec3<T>::Vec3(T x, T y, T z) : x(x), y(y), z(z) {}

template <typename T>
inline Vec3<T>::~Vec3(){}

template <typename T>
inline T& Vec3<T>::operator [] (int i) const
{
  return (T&)((&x)[i]);
}

template <typename T>
inline Vec3<T>& Vec3<T>::operator = (const Vec3 other)
{
  for (int i=0; i<3; i++) (*this)[i] = other[i];
  return *this;
}

// Basic operation with other vector or itself
template <typename T>
inline Vec3<T> Vec3<T>::operator + (const Vec3& other) const
{
  return Vec3<T>(x+other.x, y+other.y, z+other.z);
}

template <typename T>
inline Vec3<T> Vec3<T>::operator - (const Vec3& other) const
{
  return Vec3<T>(x-other.x, y-other.y, z-other.z);
}

template <typename T>
inline Vec3<T> Vec3<T>::operator * (const Vec3& other) const
{
#ifndef SUPPRESS_WARNINGS
  cerr << "WARNING: * operator is NOT dot product between two vectors" << endl;
#endif
  return Vec3<T>(x*other.x, y*other.y, z*other.z);
}

template <typename T>
inline Vec3<T> Vec3<T>::operator / (const Vec3& other) const
{
  return Vec3<T>(x/other.x, y/other.y, z/other.z);
}

template <typename T>
inline Vec3<T> Vec3<T>::operator - () const
{
  return Vec3<T>(-x, -y, -z);
}

// Basic operations with scalar values
template <typename T>
inline Vec3<T> Vec3<T>::operator * (const T scalar)
{
  return Vec3<T>(x*scalar, y*scalar, z*scalar);
}

template <typename T>
inline Vec3<T> Vec3<T>::operator / (const T scalar)
{
  return Vec3<T>(x/scalar, y/scalar, z/scalar);
}

// Operators that change the vector itself based on an other vector
template <typename T>
inline Vec3<T>& Vec3<T>::operator += (const Vec3& other)
{
  for (int i=0; i<3; i++) (*this)[i] += other[i];
  return *this;
}

template <typename T>
inline Vec3<T>& Vec3<T>::operator -= (const Vec3& other)
{
  for (int i=0; i<3; i++) (*this)[i] -= other[i];
  return *this;
}

template <typename T>
inline Vec3<T>& Vec3<T>::operator *= (const Vec3& other)
{
  for (int i=0; i<3; i++) (*this)[i] *= other[i];
  return *this;
}

template <typename T>
inline Vec3<T>& Vec3<T>::operator /= (const Vec3& other)
{
  for (int i=0; i<3; i++) (*this)[i] /= other[i];
  return *this;
}

// Operators that change the vector itselfbased on an scalar value
template <typename T>
inline Vec3<T>& Vec3<T>::operator *= (const T scalar)
{
  for (int i=0; i<3; i++) (*this)[i] *= scalar;
  return *this;
}

template <typename T>
inline Vec3<T>& Vec3<T>::operator /= (const T scalar)
{
  for (int i=0; i<3; i++) (*this)[i] /= scalar;
  return *this;
}

// Boolean operators
template <typename T>
inline bool Vec3<T>::operator == (const Vec3& other) const
{
  return (x == other.x) && (y == other.y) && (z == other.z);
}

template <typename T>
inline bool Vec3<T>::operator != (const Vec3& other) const
{
  return (x != other.x) || (y != other.y) || (z != other.z);
}

template <typename T>
inline bool Vec3<T>::operator < (const Vec3& other) const
{
  return (Length() < other.Length());
}

// Util methods
template <typename T>
inline void Vec3<T>::Set(T x, T y, T z)
{
  this->x = x;
  this->y = y;
  this->z = z;
}

template <typename T>
inline T Vec3<T>::Length() const
{
  return sqrtf(x*x + y*y + z*z);
}

template <typename T>
inline T Vec3<T>::LengthSq() const
{
  return x*x + y*y + z*z;
}

template <typename T>
inline T Vec3<T>::Distance(const Vec3<T>& other) const
{
  return sqrtf((other.x-x) * (other.x-x) +
               (other.y-y) * (other.y-y) +
               (other.z-z) * (other.z-z));
}

template <typename T>
inline Vec3<T> Vec3<T>::Normalized() const
{
  T l = Length();
  return Vec3<T>(x/l, y/l, z/l);
}

template <typename T>
inline void Vec3<T>::Normalize()
{
  T l = Length();
  for (int i=0; i<3; i++) (*this)[i] /= l;
}

template <typename T>
inline T Vec3<T>::Dot(const Vec3<T>& other) const
{
  return (x*other.x + y*other.y + z*other.z);
}

template <typename T>
inline Vec3<T> Vec3<T>::Cross(const Vec3<T>& other) const
{
  return Vec3<T>(y*other.z - z*other.y,
                 z*other.x - x*other.z,
                 x*other.y - y*other.x);
}

template <typename T>
inline bool Vec3<T>::Equal(const Vec3<T>& other, T e) const
{
  return ((fabs(x-other.x) < e) &&
          (fabs(y-other.y) < e) &&
          (fabs(z-other.z) < e));
}

template <typename T>
inline void Vec3<T>::Print(std::ostream& os) const
{
  Vec3<T> n = Normalized();
  os << "Coordinates (x, y, z) -> (" << x << ", " << y << ", " << z << ") " << endl;
  os << "Normalized  (x, y, z) -> (" << n.x << ", " << n.y << ", " << n.z << ") " << endl;
  os << "Length = " << Length() << endl;
}

// For some more convineience
template <typename T>
Vec3<T> operator * (T a, const Vec3<T> vec)
{
  return Vec3<T>(a*vec.x, a*vec.y, a*vec.z);
}

template <typename T>
std::ostream& operator << (std::ostream& os, const Vec3<T>& vec)
{
  os << " (" << vec.x << ", " << vec.y << ", " << vec.z << ") ";
  return os;
}


#endif