﻿//----------------------------------------------------------------------------//
//    Learning OpenGL: Εισαγωγικά Μαθήματα OpenGL                             //
//                                                                            //
//    Πλατφόρμα ανάπτυξης παραδειγμάτων                                       //
//                                                                            //
//    Simple OBJ Viewer                                                       //
//----------------------------------------------------------------------------//

#include <iostream>	         // For string handling
#include <fstream>	         // For string handling
#include <sstream>           // For some string convertions
#include <random>            // Random generator
#include <stdlib.h>		       // Just for some standard functions
#include <stdio.h>           // - Just for some ASCII messages
#include <glut.h>            // - An interface and windows management library
#include "visuals.h"         // Header file for our OpenGL functions
#include "params.h"
#include "model.h"

#define KEY_ESC 27
#define PI 3.1415926
#define OBJ_CAMERA
//#define FPS_CAMERA

/*******************************************************************************
  State Variables
 ******************************************************************************/

// Model variables
extern string modelsDir;
extern string graphsDir;
extern string partitionsDir;
extern string anchorDir;
extern string boundsDir;
extern string filename1;
extern string filename2;
extern string filename3;
extern string partitionsfilename;
extern string anchorfilename1;
extern string anchorfilename2;
extern string anchorfilename3;

string filenameModel1 = modelsDir + filename1 + ".obj";
string filenameModel2 = modelsDir + filename2 + ".obj";
string filenameModel3 = modelsDir + filename3 + ".obj";

#ifdef SHOW_PARTITIONS
string filenamePartitions = partitionsDir + partitionsfilename + ".graph.part." +
  static_cast<ostringstream*>( &(ostringstream() << SHOW_PARTITIONS) )->str();
  #ifdef WRITE_BOUNDS
  string filenameBounds = boundsDir + filename1 + "_" +
    static_cast<ostringstream*>( &(ostringstream() << SHOW_PARTITIONS) )->str() +
    ".bnds";
  #endif
#endif

Model model1;
Model model2;
Model model3;
bool clockwise  = false;
bool wireframe  = false;

static Vec3f mr(0., 0, 0);
bool isRotate = false;

#ifdef SHOW_MODEL2
  #ifdef SHOW_MODEL3
    static Vec3f m1pos(-180, 0, 0);
    static Vec3f m2pos(0, 0, 0);
    static Vec3f m3pos(180, 0, 0);
  #else
    static Vec3f m1pos(-80, 0, 0);
    static Vec3f m2pos(80, 0, 0);
    static Vec3f m3pos(0, 0, 0);
  #endif
#else
  #ifdef SHOW_MODEL3
    static Vec3f m1pos(-80, 0, 0);
    static Vec3f m2pos(0, 0, 0);
    static Vec3f m3pos(80, 0, 0);
  #else
    static Vec3f m1pos(0, 0, 0);
    static Vec3f m2pos(0, 0, 0);
    static Vec3f m3pos(0, 0, 0);
  #endif
#endif

#ifdef SHOW_ANCHOR
string filenameAnchor1 = anchorDir + anchorfilename1 + ".anc";
string filenameAnchor2 = anchorDir + anchorfilename2 + ".anc";
string filenameAnchor3 = anchorDir + anchorfilename3 + ".anc";
#endif

// Camera variables
static float moveStep = 10;
static float rotFactor = 0.005;
static float moveFactor = 0.01;
static float zoomStep = 15;
static float zoomFactor = 1;
static float cdist = 250.0;
static float cx = 0.0;
static float cy = 0.0;
static float cz = cdist;
static float tx = 0.0;
static float ty = 0.0;
static float tz = 0.0;
static float azimuthAngle  = 0.0;
static float altitudeAngle = PI/2.;
static bool  showtarget = false;

// Event handle variables
static bool leftMouseClickDown  = false;
static bool rightMouseClickDown = false;
static int  mx0;
static int  my0;

/*******************************************************************************
  Implementation of Extra Functions
 ******************************************************************************/

void DrawString(const char *str, int x, int y, float color[4], void *font)
{
  glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT);
    glDisable(GL_LIGHTING);

    glColor4fv(color);
    glRasterPos2i(x, y);

    // loop all characters in the string
    while(*str)
    {
        glutBitmapCharacter(font, *str);
        ++str;
    }
  glPopAttrib();
}

//------------------------------------------------------------------------------

void ShowInfo()
{
  // Backup current model-view matrix
  glPushMatrix();
    glLoadIdentity();

    // Set to 2D orthogonal projection
    // Switch to projection matrix
    glMatrixMode(GL_PROJECTION);
    // Save current projection matrix
    glPushMatrix();                     
      glLoadIdentity();
      GLdouble width  = 809;
      GLdouble height = 504;

      gluOrtho2D(0, width, 0, height);

      float color[4] = {0.1, 0.1, 0.1, 1.0};

      DrawString("__________Parameters___________", 0, height - 1*12, color, GLUT_BITMAP_HELVETICA_12);
      DrawString("Partitions: 15", 0, height - 2*12, color, GLUT_BITMAP_HELVETICA_12);
      DrawString("Object Compress Ratio: 85.6%/83.7", 0, height - 3*12, color, GLUT_BITMAP_HELVETICA_12);
      DrawString("Submesh Compress Ratio: 75%", 0, height - 4*12, color, GLUT_BITMAP_HELVETICA_12);
      DrawString("No Anchor Pts / Partition: 25/50", 0, height - 5*12, color, GLUT_BITMAP_HELVETICA_12);
      DrawString("Decode Strategy: GroupLasso", 0, height - 6*12, color, GLUT_BITMAP_HELVETICA_12);
      DrawString("Anchor Strategy: At Edges", 0, height - 7*12, color, GLUT_BITMAP_HELVETICA_12);
      DrawString("______________________________", 0, height - 8*12, color, GLUT_BITMAP_HELVETICA_12);

      // restore projection matrix
    glPopMatrix(); 

    // restore modelview matrix
    glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
}

//------------------------------------------------------------------------------

void MessageDisplay()
{
  cout << "__________________Controls_____________________" << endl;
  cout << "| W: wireframe                                |" << endl;
  cout << "| S: solid                                    |" << endl;
  cout << "| A: show Anchor                              |" << endl;
  cout << "| Y: auto rotate                              |" << endl;
  cout << "| C: toggle CCW/CW                            |" << endl;
  cout << "| Ctr/Alt + Arrows/PgUp/PgDn: Move model(s)   |" << endl;
  cout << "| LMC + Mouse motion: rotate camera           |" << endl;
  cout << "| RMC + Mouse motion: zoom in/out             |" << endl;
  cout << "| +: zoom in                                  |" << endl;
  cout << "| -: zoom out                                 |" << endl;
  cout << "| T: toggle show cameras' target              |" << endl;
  cout << "| #1-3: Set model# as target                  |" << endl;
  cout << "|_____________________________________________|" << endl;
}

/*******************************************************************************
  Implementation of Visual Functions
 ******************************************************************************/

void Setup()
{
  MessageDisplay();

  // Set up parameters

  // polygon rendering mode and material properties
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
  // Make models with smooth edges
  glShadeModel(GL_SMOOTH);

  // Points represented as circles
  glEnable(GL_POINT_SMOOTH);
  glEnable(GL_LINE_SMOOTH);
  
  // Blending
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Depth test
  glEnable(GL_DEPTH_TEST);
  // Renders a fragment if its z value is less or equal of the stored value
  glDepthFunc(GL_LEQUAL);
  glClearDepth(1);
  
  // Cull Faces
  // Don't draw triangles that cannot be seen
  glEnable(GL_CULL_FACE);
  if (clockwise) glFrontFace(GL_CW);
  else glFrontFace(GL_CCW);

  // Polygon rendering mode enable Material
  glEnable(GL_COLOR_MATERIAL);
  // We use Ambient and Diffuse color of each object as given in color3f
  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

  // Enable the Lighting
  glEnable(GL_LIGHTING);

  // Set up light source
  // Light 0
  GLfloat light0Ambient[]   = {0.2, 0.2, 0.2, 1.0};
  GLfloat light0Diffuse[]   = {0.6, 0.6, 0.6, 1.0};
  GLfloat light0Specular[]  = {0.9, 0.9, 0.9, 1.0};
  GLfloat light0Position[]  = {0.0, 0.0, 0.0, 1.0};
  
  // Set Ambient Light
  glLightfv(GL_LIGHT0, GL_AMBIENT,  light0Ambient);
  // Set Difuse Light
  glLightfv(GL_LIGHT0, GL_DIFFUSE,  light0Diffuse);
  // Set Specular Light
  glLightfv(GL_LIGHT0, GL_SPECULAR, light0Specular);
  // Set Position of the Light
  glLightfv(GL_LIGHT0, GL_POSITION, light0Position);
  
  // Enable Light
  glEnable(GL_LIGHT0);

  // Read OBJ model
  model1.LoadOBJ(filenameModel1);

#ifdef SHOW_MODEL2
  model2.LoadOBJ(filenameModel2);
#endif

#ifdef SHOW_MODEL3
  model3.LoadOBJ(filenameModel3);
#endif

#ifdef SHOW_PARTITIONS
  model1.LoadPartitions(filenamePartitions);
  #ifdef SHOW_MODEL2
  model2.LoadPartitions(filenamePartitions);
  #endif
  #ifdef SHOW_MODEL3
  model3.LoadPartitions(filenamePartitions);
  #endif
#ifdef WRITE_BOUNDS
  model1.WriteBounds(filenameBounds);
#endif
#else
  #ifdef WRITE_BOUNDS
    cout << "WARRNING: You have to enable partitions first!!!" << endl;
  #endif
#endif

#ifdef SHOW_ANCHOR
  model1.LoadAnchorPoints(filenameAnchor1);
  model2.LoadAnchorPoints(filenameAnchor2);
  model3.LoadAnchorPoints(filenameAnchor3);
#endif


  // Black background
  glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
}

//------------------------------------------------------------------------------

void Render()
{
  // Clean up the colour of the window and the depth buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  ShowInfo();

  // Set the camera
  gluLookAt(cx, cy, cz, // camera position
            tx, ty, tz, // target position
            0,  1,  0); // up vector
  
  // Show target
  if(showtarget)
  {
    glColor3f(0.8, 0.4, 0.4);
    glPushMatrix();
      glTranslatef(tx, ty, tz);
      glutSolidSphere(1, 9, 9);
    glPopMatrix();
  }

  if (clockwise) glFrontFace(GL_CW);
  else glFrontFace(GL_CCW);
  if(wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
  glColor3f(0.7, 0.7, 0.7);
  glPushMatrix();
    glTranslatef(m1pos.x, m1pos.y, m1pos.z);
#ifdef OBJ_CAMERA
    glRotatef(mr.y, 0, 1, 0);
    glRotatef(mr.x, 1, 0, 0);
#endif
    model1.Draw();
  glPopMatrix();
  
#ifdef SHOW_MODEL2
  glColor3f(0.2, 0.7, 0.2);
  glPushMatrix();
    glTranslatef(m2pos.x, m2pos.y, m2pos.z);
#ifdef OBJ_CAMERA
    glRotatef(mr.y, 0, 1, 0);
    glRotatef(mr.x, 1, 0, 0);
#endif
    model2.Draw();
  glPopMatrix();
#endif

#ifdef SHOW_MODEL3
  glColor3f(0.2, 0.7, 0.2);
  glPushMatrix();
    glTranslatef(m3pos.x, m3pos.y, m3pos.z);
#ifdef OBJ_CAMERA
    glRotatef(mr.y, 0, 1, 0);
    glRotatef(mr.x, 1, 0, 0);
#endif
    model3.Draw();
  glPopMatrix();
#endif

  //*/
  // All drawing commands applied to the
  // hidden buffer, so now, bring forward
  // the hidden buffer and hide the visible one
  glutSwapBuffers();
}

//------------------------------------------------------------------------------

void Resize(int w, int h)
{
  glMatrixMode(GL_PROJECTION); 
  // define the visible area of the window ( in pixels )
  if (h==0) h=1;
  glViewport(0, 0, w, h);

  // Setup viewing volume
  glLoadIdentity();
 
  gluPerspective(60.0, (float)w/(float)h, 1.0, 1000.0);
}

//------------------------------------------------------------------------------

void Idle()
{
  glMatrixMode(GL_MODELVIEW);

  // Make your changes here
  if (isRotate) mr.y -= 1.8;
  // After your changes rerender
  glutPostRedisplay();
}

//------------------------------------------------------------------------------

void MenuSelect(int choice)
{
}

//------------------------------------------------------------------------------

void KeyboardDown(unsigned char key, int x, int y)
{
  switch(key)
  {
    case 'W':
    case 'w':
      wireframe = true;
      break;
    case 'S':
    case 's':
      wireframe = false;
      break;
    case 'Y':
    case 'y':
      isRotate = !isRotate;
      break;
#ifdef SHOW_ANCHOR
    case 'A':
    case 'a':
      model1.showAnchor = !model1.showAnchor;
      model2.showAnchor = !model2.showAnchor;
      model3.showAnchor = !model3.showAnchor;
      break;
#endif
    case 'C':
    case 'c':
      clockwise = !clockwise;
      break;
    case 'I':
    case 'i':
#ifdef FPS_CAMERA
      {
      float dx = (tx-cx) * moveFactor;
      float dy = (ty-cy) * moveFactor;
      float dz = (tz-cz) * moveFactor;
      cx += dx;
      tx += dx;
      cy += dy;
      ty += dy;
      cz += dz;
      tz += dz;
      }
#endif
      break;
    case 'K':
    case 'k':
#ifdef FPS_CAMERA
      {
      float dx = (tx-cx) * moveFactor;
      float dy = (ty-cy) * moveFactor;
      float dz = (tz-cz) * moveFactor;
      cx -= dx;
      tx -= dx;
      cy -= dy;
      ty -= dy;
      cz -= dz;
      tz -= dz;
      }
#endif
      break;
    case 'J':
    case 'j':
#ifdef FPS_CAMERA
      {
      float dx = (tz-cz) * moveFactor;
      float dz =-(tx-cx) * moveFactor;
      cx += dx;
      tx += dx;
      cz += dz;
      tz += dz;
      }
#endif
      break;
    case 'L':
    case 'l':
#ifdef FPS_CAMERA
      {
      float dx = (tz-cz) * moveFactor;
      float dz =-(tx-cx) * moveFactor;
      cx -= dx;
      tx -= dx;
      cz -= dz;
      tz -= dz;
      }
#endif
      break;
    case '+':
      {
      cdist -= zoomStep;
      cx -= zoomStep*sin(altitudeAngle)*sin(azimuthAngle);
      cy -= zoomStep*cos(altitudeAngle);
      cz -= zoomStep*sin(altitudeAngle)*cos(azimuthAngle);
      }
      break;
    case '-':
      cdist += zoomStep;
      cx += zoomStep*sin(altitudeAngle)*sin(azimuthAngle);
      cy += zoomStep*cos(altitudeAngle);
      cz += zoomStep*sin(altitudeAngle)*cos(azimuthAngle);
      break;
    case 'R':
    case 'r':
      cdist = 250.0;
      tx = 0.0;
      ty = 0.0;
      tz = 0.0;
      cx = tx + cdist*sin(altitudeAngle)*sin(azimuthAngle);
      cy = ty + cdist*cos(altitudeAngle);
      cz = tz + cdist*sin(altitudeAngle)*cos(azimuthAngle);
      break;
    case 'T':
    case 't':
      showtarget = !showtarget;
      break;
    case '0':
      tx = m1pos.x + m2pos.x + m3pos.x;
      ty = m1pos.y + m2pos.y + m3pos.y;
      tz = m1pos.z + m2pos.z + m3pos.z;
#ifdef OBJ_CAMERA
      cx = tx;
      cy = ty;
      cz = tz + cdist;
#else
      cx = tx + cdist*sin(altitudeAngle)*sin(azimuthAngle);
      cy = ty + cdist*cos(altitudeAngle);
      cz = tz + cdist*sin(altitudeAngle)*cos(azimuthAngle);
#endif
      break;
    case '1':
      tx = m1pos.x;
      ty = m1pos.y;
      tz = m1pos.z;
#ifdef OBJ_CAMERA
      cx = tx;
      cy = ty;
      cz = tz + cdist;
#else
      cx = tx + cdist*sin(altitudeAngle)*sin(azimuthAngle);
      cy = ty + cdist*cos(altitudeAngle);
      cz = tz + cdist*sin(altitudeAngle)*cos(azimuthAngle);
#endif
      break;
    case '2':
      tx = m2pos.x;
      ty = m2pos.y;
      tz = m2pos.z;
#ifdef OBJ_CAMERA
      cx = tx;
      cy = ty;
      cz = tz + cdist;
#else
      cx = tx + cdist*sin(altitudeAngle)*sin(azimuthAngle);
      cy = ty + cdist*cos(altitudeAngle);
      cz = tz + cdist*sin(altitudeAngle)*cos(azimuthAngle);
#endif
      break;
    case '3':
      tx = m3pos.x;
      ty = m3pos.y;
      tz = m3pos.z;
#ifdef OBJ_CAMERA
      cx = tx;
      cy = ty;
      cz = tz + cdist;
#else
      cx = tx + cdist*sin(altitudeAngle)*sin(azimuthAngle);
      cy = ty + cdist*cos(altitudeAngle);
      cz = tz + cdist*sin(altitudeAngle)*cos(azimuthAngle);
#endif
      break;
    case KEY_ESC:
      exit(0);
      break;
    default:
      break;
  }
  glutPostRedisplay();
}

//------------------------------------------------------------------------------

void KeyboardUp(unsigned char key, int x, int y)
{
}

//------------------------------------------------------------------------------

void KeyboardSpecialDown(int key, int x, int y)
{
  int modifier = glutGetModifiers();
  switch(key)
  {
    case GLUT_KEY_UP:
      cout << "up key pressed" << endl;
      if(modifier & GLUT_ACTIVE_CTRL)  m1pos.z   -= moveStep;
      if(modifier & GLUT_ACTIVE_ALT)   m2pos.z -= moveStep;
      if(modifier & GLUT_ACTIVE_SHIFT) m3pos.z -= moveStep;
      break;
    case GLUT_KEY_DOWN:
      if(modifier & GLUT_ACTIVE_CTRL)  m1pos.z   += moveStep;
      if(modifier & GLUT_ACTIVE_ALT)   m2pos.z += moveStep;
      if(modifier & GLUT_ACTIVE_SHIFT) m3pos.z += moveStep;
      break;
    case GLUT_KEY_RIGHT:
      cout << "right key pressed" << endl;
      if(modifier & GLUT_ACTIVE_CTRL)  m1pos.x   += moveStep;
      if(modifier & GLUT_ACTIVE_ALT)   m2pos.x += moveStep;
      if(modifier & GLUT_ACTIVE_SHIFT) m3pos.x += moveStep;
      break;
    case GLUT_KEY_LEFT:
      cout << "left key pressed" << endl;
      if(modifier & GLUT_ACTIVE_CTRL)  m1pos.x   -= moveStep;
      if(modifier & GLUT_ACTIVE_ALT)   m2pos.x -= moveStep;
      if(modifier & GLUT_ACTIVE_SHIFT) m3pos.x -= moveStep;
      break;
    case GLUT_KEY_PAGE_UP:
      cout << "page up key pressed" << endl;
      if(modifier & GLUT_ACTIVE_CTRL)  m1pos.y   += moveStep;
      if(modifier & GLUT_ACTIVE_ALT)   m2pos.y += moveStep;
      if(modifier & GLUT_ACTIVE_SHIFT) m3pos.y += moveStep;
      break;
    case GLUT_KEY_PAGE_DOWN:
      cout << "page down key pressed" << endl;
      if(modifier & GLUT_ACTIVE_CTRL)  m1pos.y   -= moveStep;
      if(modifier & GLUT_ACTIVE_ALT)   m2pos.y -= moveStep;
      if(modifier & GLUT_ACTIVE_SHIFT) m3pos.y -= moveStep;
      break;
    default:
      break;
  }
  glutPostRedisplay();
  //*/
}

//------------------------------------------------------------------------------

void KeyboardSpecialUp(int key, int x, int y)
{
}

//------------------------------------------------------------------------------

void MouseClick(int button, int state, int x, int y)
{
  if(state == GLUT_DOWN)
  {
    mx0 = x;
    my0 = y;
    if(button == GLUT_LEFT_BUTTON)
      leftMouseClickDown = true;
    else
      leftMouseClickDown = false;

    if(button == GLUT_RIGHT_BUTTON)
      rightMouseClickDown = true;
    else
      rightMouseClickDown = false;
  }
}

//------------------------------------------------------------------------------

void MouseMotion(int x, int y)
{
  // Mouse dx, dy
  int dx = x - mx0;
  int dy = y - my0;

  if (leftMouseClickDown)
  {
    // Calculate angles
    azimuthAngle  -= dx*rotFactor;
    altitudeAngle -= dy*rotFactor;
  }
  if(rightMouseClickDown)
  {
    // Calculate distance
    cdist += dy*zoomFactor;
#ifdef FPS_CAMERA
    // Set new camera position (FPS camera)
    cx += dy*zoomFactor*sin(altitudeAngle)*sin(azimuthAngle);
    cy += dy*zoomFactor*cos(altitudeAngle);
    cz += dy*zoomFactor*sin(altitudeAngle)*cos(azimuthAngle);
#endif
  }

#ifdef OBJ_CAMERA
  mr.x = 90 - (altitudeAngle*180/PI);
  mr.y = -azimuthAngle*180/PI;

  cx = tx;
  cy = ty;
  cz = tz + cdist;
#else
  #ifdef FPS_CAMERA
    // Set new target position (FPS camera)
    tx = cx - cdist*sin(altitudeAngle)*sin(azimuthAngle);
    ty = cy - cdist*cos(altitudeAngle);
    tz = cz - cdist*sin(altitudeAngle)*cos(azimuthAngle);
  #else
    // Trackball camera
    cx = tx + cdist*sin(altitudeAngle)*sin(azimuthAngle);
    cy = ty + cdist*cos(altitudeAngle);
    cz = tz + cdist*sin(altitudeAngle)*cos(azimuthAngle);
  #endif
#endif

  // Keep mouse x,y for next call
  mx0 = x;
  my0 = y;

  glutPostRedisplay();
}
