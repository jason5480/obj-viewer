#ifndef _Params_H_
#define _Params_H_

#include <iostream>

/*******************************************************************************
  Set program parameters
 ******************************************************************************/

//#define SHOW_MODEL2
//#define SHOW_MODEL3

#define SHOW_PARTITIONS 15
//#define SHOW_ANCHOR
//#define WRITE_BOUNDS

// Directories to load the model, partition, and anchor points files
std::string modelsDir = "../Models/";;
std::string partitionsDir = "../Partitions/";
std::string anchorDir = "../Anchor/";
// Directories to write the graph and bounds files
std::string graphsDir = "../Graphs/";
std::string boundsDir = "../Bounds/";

// Model file names
std::string filename1 = "tyra_low";
std::string filename2 = "tyra_low_P15_CR35_M45_GL_U";
std::string filename3 = "tyra_low_P15_CR35_M45_GL_U_smth";
// Partitions file name
std::string partitionsfilename = "tyra_low";
// Anchor file name
std::string anchorfilename1 = "tyra_low_E";
std::string anchorfilename2 = "tyra_low_E";
std::string anchorfilename3 = "tyra_low_E";


#endif