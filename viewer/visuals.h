#ifndef _Visuals_H_
#define _Visuals_H_

/*******************************************************************************
  Visual Functions
 ******************************************************************************/

// Set up the OpenGL state machine and create a light source
void Setup();

// The function responsible for drawing everything in the
// OpenGL context associated to a window.
void Render();

// Handle the window size changes and define the world coordinate
// system and projection type
void Resize(int w, int h);

// Idle function
void Idle();

// Function for menu select events
void MenuSelect(int choice);

// Function for handling keyboard events.
void KeyboardDown(unsigned char key, int x, int y);

// Function for handling keyboard events.
void KeyboardUp(unsigned char key, int x, int y);

// Function for handling keyboard events.
void KeyboardSpecialDown(int key, int x, int y);

// Function for handling keyboard events.
void KeyboardSpecialUp(int key, int x, int y);

// Function for handling mouse events
void MouseClick(int button, int state, int x, int y);

// Function for handling mouse events
void MouseMotion(int x, int y);


#endif