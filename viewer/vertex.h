#ifndef _Vertex_H_
#define _Vertex_H_

//----------------------------------------------------------------------------//
//    Learning OpenGL: ���������� �������� OpenGL                             //
//    ��������� ��������� �������������                                       //
//    Class: 3D Vertex                                                        //
//    Author: Iason Nikolas                                                   //
//    For any bug/suggestions mail to: iason.nikolas@ece.upatras.gr           //
//----------------------------------------------------------------------------//

#include "vec3.h"
#include <vector>

/*******************************************************************************
  Vertex
 ******************************************************************************/

template <typename T>
class Vertex
{
public:
  // ctors
  Vertex();
  Vertex(Vertex& other);
  Vertex(const Vertex& other);
  Vertex(Vec3<T>& p, int par=-1);

  ~Vertex();

  // Data members
  Vec3<T> pos;
  int     part;
  std::vector<int> adjacentVerticies;
  std::vector<int> adjacentTriangles;
};

template <typename T>
inline Vertex<T>::Vertex() : pos(), part(-1)
{}

template <typename T>
inline Vertex<T>::Vertex(Vertex& other) : pos(other.pos), part(other.part)
{}

template <typename T>
inline Vertex<T>::Vertex(const Vertex& other) : pos(other.pos), part(other.part)
{}

template <typename T>
inline Vertex<T>::Vertex(Vec3<T>& p, int par) : pos(p), part(par)
{}

template <typename T>
inline Vertex<T>::~Vertex()
{}

template <typename T>
std::ostream& operator << (std::ostream& os, const Vertex<T>& v)
{
  os << " Pos: " << v.pos << " Norm: " << v.norm << " Part: " << v.part << " AjTringles: ";
  for (size_t i = 0; i < adjacentTriangles.size(); i++) os << adjacentTriangles[1] << "\t";
  os << "AjVertices: ";
  for (size_t i = 0; i < adjacentVertices.size(); i++) os << adjacentVertices[1] << "\t";
  return os;
}


#endif