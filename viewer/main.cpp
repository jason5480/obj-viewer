﻿//----------------------------------------------------------------------------//
//    Learning OpenGL: Εισαγωγικά Μαθήματα OpenGL                             //
//                                                                            //
//    Πλατφόρμα ανάπτυξης παραδειγμάτων                                       //
//                                                                            //
//    Simple OBJ Viewer                                                       //
//----------------------------------------------------------------------------//

#include <stdlib.h>		// Just for some standard functions
#include <stdio.h>    // - Just for some ASCII messages
#include <glut.h>     // - An interface and windows management library
#include "visuals.h"  // Header file for our OpenGL functions

/*******************************************************************************
  Main Program code
 ******************************************************************************/

int main(int argc, char* argv[])
{
  // initialize GLUT library state
  glutInit(&argc, argv);

  // Set up the display using the GLUT functions to
  // get rid of the window setup details:
  // - Use true RGB colour mode ( and transparency )
  // - Enable double buffering for faster window update
  // - Allocate a Depth-Buffer in the system memory or
  //   in the video memory if 3D acceleration available
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);


  // Define the main window size and initial position
  // ( upper left corner, boundaries included )
  glutInitWindowSize(896, 504);
  glutInitWindowPosition(50, 50);

  // Create and label the main window
  glutCreateWindow("Simple OBJ Viewer");

  // Configure various properties of the OpenGL rendering context
  Setup();

  // Callbacks for the GL and GLUT events:
  // Set the rendering function
  glutDisplayFunc(Render);
  // Set the resize function
  glutReshapeFunc(Resize);
  // Set the idle function
  glutIdleFunc(Idle);
  
  // Set functions for keyboard
  glutKeyboardFunc(KeyboardDown);
  glutKeyboardUpFunc(KeyboardUp);
  glutSpecialFunc(KeyboardSpecialDown);
  glutSpecialUpFunc(KeyboardSpecialUp);

  // Set functions for mouse
  glutMouseFunc(MouseClick);
  glutMotionFunc(MouseMotion);

  //Enter main event handling loop
  glutMainLoop();
  return 0;
}

